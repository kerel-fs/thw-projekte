# THW (nahe) Projekte

Eine Sammlung kostenlos nutzbarer und frei zugänglicher Projekte mit THW Bezug.

* [COVID-19](#covid-19)
* [Daten](#daten)
* [Programme](#programme)
* [Taktische Zeichen & Führungsinstrumente](#taktische-zeichen-&-führungsinstrumente)
* [Vorlagen](#vorlagen)
* [Webseiten](#webseiten)

## COVID-19
* [Corona Dashboard](https://corona.thw-bornheim.de/) - Dashboard mit Übersicht der Pandemieschutzphasen der RBs. ([Source Code](https://gitlab.com/bigo8525/thw-corona-dashboard))
* [Dokumentvorlagen für dolibarr ERP](https://git.blubbfish.net/Php/dolibarr) - ([Video](https://media.ccc.de/v/froscon2020-2582-open_source_im_katastrophenschutz))

## Daten
* [Dienststellen Scraper Extended](https://gitlab.com/Manuel_Raven/dienststellen-scraper-extended) - Kontaktdaten aller Dienststellen inkl. erweiterte Datensätze z.B. Einheiten.
* [THW Dienststellen](https://git.ovcms.thw.de/git-api-aggregation/thw-dienststellen) - Kontaktdaten aller THW Dienststellen als JSON.

## Programme
* [eStab](https://www.estab.de/) - Der Elektronischer Nachrichtenvordruck. ([Source Code](https://sourceforge.net/projects/estab/))
* [Stashcat API Client](https://gitlab.com/aeberhardt/stashcat-api-client) - Einige Hilfsfunktionen um mit der API des THW-Messengers zu reden.
* [thw-inventory](https://github.com/mziech/thw-inventory) - Ein auf die Bedürfnisse des THW zugeschnittenes Inventarsystem.
* [THWatch](https://github.com/farhaven/THWatch) - Grabs new courses from THW's last minute course listing.
* [thwebgis](https://gitlab.meier-tkn.de/thw/webgis/thwebgis) - Portables webbasiertes GIS, auch für den Offline-Betrieb geeignet.
* [THWin2iCalendar](https://github.com/real-or-random/thwin2icalendar) - Konvertiert THW-Dienstpläne ins iCalendar-Format.
* [top_downloader](https://github.com/dmth/top_downloader) - Bulk-downloads DTK maps in PDF-format from https://www.opengeodata.nrw.de/produkte/geobasis/tk

## Taktische Zeichen & Führungsinstrumente
* [Taktische Zeichen Vorlagen Generator](https://taktische-zeichen.org/) - Erstelle Druckvorlagen für Taktische Zeichen in einem einfachen Web UI. ([Source Code](https://gitlab.com/tristanlins/taktische-zeichen-vorlagen-generator))
* [Taktische-Zeichen](https://github.com/jonas-koeritz/Taktische-Zeichen) - Taktische Zeichen für Hilfsorganisationen als Vektorgrafiken.
* [THW Magnete](https://thw-magnete.de/) - Druckvorlagen für Magnete und Führungsinstrumente.

## Vorlagen
* [thw-agtbook](https://github.com/rwolke/thw-agtbook) - Atemschutznachweis für Tätigkeit unter Atemschutz.
* [thwbeamer](https://github.com/azhural/thwbeamer) - LaTeX beamer template for THW presentations.

## Webseiten
* [EGS Visualisierung](https://rwolke.github.io/thw-egs/) - Visualisierung von EGS Konstruktionen. ([Source Code](https://github.com/rwolke/thw-egs))
* [JApp](https://japp.thw-jugend.de) - Ideen für Jugend-Dienste und das Leistungsabzeichen der THW Jugend.
* [THW OV-CMS](https://doku.ov-cms.thw.de) - TYPO3-CMS der THW-Ortsverbände
* [THW Theorie](http://rwolke.github.io/thw-theorie/) - WebApp zum Lernen für die THW-Theorie Prüfung. ([Source Code](https://github.com/rwolke/thw-theorie))
* [THW-Adressbuch](https://thw-adressbuch.meldestein.de) - Durchsuchbare Liste aller Dienststellen. Als PWA offline installierbar. ([Source Code](https://gitlab.com/Manuel_Raven/thw-adressbuch))
* [THW-Links](https://github.com/hdrees/THW-Links)
* [THWiki](https://thwiki.org)
